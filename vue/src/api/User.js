import Api from "./Api";

export default {
    login(form) {
        return Api().post("/sanctum/token", form);
    },

    logout() {
        return Api().post("/logout");
    },

    auth() {
        return Api().get("/user");
    },

    getIndex() {
        return Api().get("/guestbook/index");
    },

    getAll() {
        return Api().get("/guestbook/all");
    },

    sendQuestion(question){
        return Api().post("/guestbook/question", question).then(response =>
            alert(response.data)
        );
    },

    sendAnswer(answer){
        return Api().post("/guestbook/answer", answer)
    },

    delete(id){
        return Api().delete("/guestbook/delete/" + id)
    }

};
