<?php

namespace App\Http\Controllers\Api;

use App\GuestBook;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RESTController extends Controller
{
    public function index()
    {
        return GuestBook::whereNotNull('answer')->orderByDesc('created_at')->get();
    }

    public function all()
    {
        if (Auth::user()->role == 'moderator')
            return GuestBook::orderByDesc('created_at')->get();
        else
            return response('Auth error', 500);
    }

    public function question(Request $request)
    {
        $question = $request->question;
        if (!empty($question)) {
            if (GuestBook::create(['question' => $question]))
                return response('Question accepted', 200);
        }
        return response('Error', 500);

    }

    public function answer(Request $request)
    {
        if (Auth::user()->role == 'moderator') {
            $id = $request->id;
            $answer = $request->answer;
            if ($item = GuestBook::find($id)) {
                $item->answer = $answer;
                $item->save();
                return response('ok', 200);
            } else
                return response('error', 500);
        } else
            return response('Auth error', 500);
    }

    public function delete(Request $request)
    {
        if (Auth::user()->role == 'moderator') {
            $id = $request->id;
            if ($item = GuestBook::find($id)) {
                $item->delete();
                return response('ok', 200);
            } else
                return response('error', 500);
        } else
            return response('Auth error', 500);
    }
}
