
## Install
composer
```
composer install
```
.env
```
rename .env.example in .env and write data base parameters
```
key generate
```
php artisan key:generate
```
init data base
```
php artisan migrate --seed
```
init npm
```
npm install
npm run dev
```
run server
```
php artisan serve
```
