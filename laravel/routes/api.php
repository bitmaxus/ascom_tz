<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/sanctum/token', 'Api\SanctumAuth@login');
Route::prefix('guestbook')->group(function () {
    Route::get('/index', 'Api\RESTController@index');
    Route::post('/question', 'Api\RESTController@question');
});

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::post('/logout', 'Api\SanctumAuth@logout');

    Route::prefix('guestbook')->group(function () {
        Route::get('/all', 'Api\RESTController@all');
        Route::post('/answer', 'Api\RESTController@answer');
        Route::delete('/delete/{id}', 'Api\RESTController@delete');
    });
});

