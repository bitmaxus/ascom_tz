<?php

use Illuminate\Database\Seeder;
use App\GuestBook;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'admin',
            'role' => 'moderator',
            'email' => 'gmaxus@gmail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            ]);

        factory(GuestBook::class,15)->create();
    }
}
