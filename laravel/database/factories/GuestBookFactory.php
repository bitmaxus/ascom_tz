<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\GuestBook;
use Faker\Generator as Faker;


$factory->define(GuestBook::class, function (Faker $faker) {
    return [
        'question' => $faker->text,
        'answer' => $faker->text,
        'created_at' => $faker->dateTimeBetween('-5days','now'),
    ];
});
